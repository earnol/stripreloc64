========================================================================
			    StripReloc64 
========================================================================


   StripReloc64 v1.0
      Strip relocation section from Win32 and Win64 PE files
      Copyright(C) 2015 Eänolituri Lómitaurë. Based on Jordan Russell 
      (http://www.jrsoftware.org/) StripReloc utility for Win32 PE files.

   This program is free software; you can redistribute it and / or
      modify it under the terms of the GNU General Public License
      as published by the Free Software Foundation; either version 2
      of the License, or(at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111 - 1307, USA.

      Build instructions:
      Please use Visual Studio 2013 to build it. Express edition is ok.
