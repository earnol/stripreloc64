﻿/*
   StripReloc64 v1.0
      Strip relocation section from Win32 and Win64 PE files
      Copyright(C) 2015  Eänolituri Lómitaurë. Based on Jordan Russell (http://www.jrsoftware.org/) StripReloc utility.

   This program is free software; you can redistribute it and / or
      modify it under the terms of the GNU General Public License
      as published by the Free Software Foundation; either version 2
      of the License, or(at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111 - 1307, USA.
*/
   


#include "stdafx.h"
#include <stdlib.h>
#include <io.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/utime.h>
#include <windows.h>
#include <Imagehlp.h>
#include <string>
#include <memory>
#include <exception>  
#include <vector>

#pragma warning(disable: 4996)
#ifdef _UNICODE
typedef std::wstring tstring;
#else
typedef std::string tstring;
#endif

const TCHAR *Version = _T("1.0");
bool KeepBackups = true;
bool WantValidChecksum= false;
bool ForceStrip = false;
bool Is64Bit = false;
HANDLE ImageHlpHandle;
void Strip(tstring const &Filename);

int _tmain(int argc, _TCHAR* argv[])
{

  int P;
  size_t I;
  bool HasFileParameter = false;
  int NumFiles = 0;
  std::vector<tstring> FilesList;
  _tcprintf(_T("StripReloc v. %s, Copyright (C) 2015 Eänolituri Lómitaurë\n"), Version);
  if (argc == 1)
  {
    _tcprintf(_T("Strip relocation section from Win32 PE files\n"));
    _tcprintf(_T("\n"));
  Show_help:
    TCHAR fn[_MAX_FNAME], fe[_MAX_EXT];
    _tsplitpath(argv[0], NULL, NULL, fn, fe);
    _tcprintf(_T("usage:     %s%s [switches] filename.exe\n"), fn, fe);
    _tcprintf(_T("\n"));
    _tcprintf(_T("switches:  /B  don't create .bak backup files\n"));
    _tcprintf(_T("           /C  write a valid checksum in the header (instead of zero)\n"));
    _tcprintf(_T("           /F  force stripping DLLs instead of skipping them. do not use!\n"));
    exit(1);
  }
  _tcprintf(_T("\n"));

  for (P = 1; P < argc; P++)
  {
    tstring S(argv[P]);
    if (S[0] != '/')
      continue;
    S.replace(0, 1, _T(""));
    I = 0;
    while (I < S.length())
    {
      switch (toupper(S[I]))
      {
        case '?': goto Show_help;
        case 'B':
        {
          KeepBackups = false;
          if (I < S.length())
          {
            // For backward compatibility, do keep backups if the character following 'B' is a '+'.
            if (S[I + 1] == '+')
            {
              KeepBackups = true;
              I++;
            }
            else
            {
              if (S[I + 1] == '-')
              {
                I++;
              }
            }
          }
          break;
        }
        case 'C':
        {
          if (!WantValidChecksum)
          {
            ImageHlpHandle = LoadLibrary(_T("imagehlp.dll"));
            if (ImageHlpHandle == NULL)
            {
              _tcprintf(_T("Error: Unable to load imagehlp.dll."));
              _tcprintf(_T("       It is required when using the /C parameter."));
              exit(1);
            }
            WantValidChecksum = true;
          }
          break;
        }
        case 'F':
        {
          ForceStrip = true;
          break;
        }
        default:
        {
          _tcprintf(_T("Invalid parameter: /%c"), S[I]);
          exit(1);
        }
      }
      I++;
    }
  }

  for (P = 1; P < argc; P++)
  {
    tstring S(argv[P]);
    if (S[0] == '/')
      continue;
    HasFileParameter = true;
    FilesList.clear();
    if (_tcschr(S.c_str(), _T('*')) == NULL && _tcschr(S.c_str(), _T('?')) == NULL)
    {
      struct __stat64 buf;
      int result;
      if (-1 != (result = _tstat64(S.c_str(), &buf)))
      {
        if (buf.st_mode & _S_IFDIR)
        {
        LInvalidFile:
          _tcprintf(_T("%s: File not found."), S.c_str());
          continue;
        }
        FilesList.push_back(S);
      }
      else
      {
        goto LInvalidFile;
      }
    }
    else
    {
      intptr_t hFind;
      struct _tfinddata64_t FindFileData;

      if ((hFind = _tfindfirst64(S.c_str(), &FindFileData)) != -1)
      {
        do
        {
          TCHAR *p;
          if ((p = _tcsstr(FindFileData.name, _T(".bak"))) != NULL)
          {
            if ((FindFileData.name - p) == (_tcslen(FindFileData.name) - _tcslen(_T(".bak"))))
            {
              continue;
            }
          }
          FilesList.push_back(FindFileData.name);
        }
        while (0 == _tfindnext64(hFind, &FindFileData));
        _findclose(hFind);
      }
      else
      {
        _tcprintf(_T("No files matching %s%s'"), S, _T("' found.\n"));
        continue;
      }
    }
    for (I = 0; I < FilesList.size(); I++)
    {
      try
      {
        Strip(FilesList[I]);
      }
      catch (tstring const & s)
      {
        _tcprintf(_T("Fatal error: %s\n"), s.c_str());
        exit(3);
      }
    }
    NumFiles++;
  }
  if (!HasFileParameter)
  {
    goto Show_help;
  }
  if (NumFiles == 0)
  {
    exit(2);
  }
  return 0;
}

DWORD CalcChecksum(HANDLE const FileHandle)
{
  DWORD Size;
  HANDLE H;
  PVOID M;
  DWORD OldSum;
  DWORD Result = 0;

  Size = GetFileSize(FileHandle, NULL);     
  H = CreateFileMapping(FileHandle, NULL, PAGE_READONLY, 0, Size, NULL);
  if (H == NULL)
  {
    throw tstring(_T("File not found..."));
  }
  try
  {
    M = MapViewOfFile(H, FILE_MAP_READ, 0, 0, Size);
    if (M == NULL)
      throw tstring(_T("Can't map..."));
    try
    {
      DWORD Result;
      if (CheckSumMappedFile(M, Size, &OldSum, &Result) == NULL)
      {
        throw tstring(_T("Error in checksum"));
      }
    }
    catch (tstring const & s)
    {
      UnmapViewOfFile(M);
      throw s;
    }
    UnmapViewOfFile(M);
  }
  catch (tstring const & s)
  {
    CloseHandle(H);
    throw s;
  }
  CloseHandle(H);
  return Result;
}

DWORD CalcChecksum(tstring const &fanme)
{
  HANDLE hFile = CreateFile(fanme.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  if (hFile != INVALID_HANDLE_VALUE)
  {
    DWORD ret = CalcChecksum(hFile);
    CloseHandle(hFile);
    return ret;
  }
  return 0;
}

__int64 FileSize(FILE *f)
{
  struct __stat64 buf;
  int result;
  if (-1 == (result = _fstat64(_fileno(f), &buf)))
    return result;
  return buf.st_size;
}

template <typename T> void Dec(T &a, T const b)
{
  a = (T)(static_cast<INT_PTR>(a)-static_cast<INT_PTR>(b));
}

inline bool FileExists(tstring const& name)
{
  struct __stat64 buff;
  return (_tstat64(name.c_str(), &buff) == 0);
}

void Strip(tstring const &Filename)
{
  typedef std::unique_ptr<IMAGE_SECTION_HEADER> PPESectionHeaderArray;
  tstring BackupFilename(_T(""));
  FILE *F, *F2;
  WORD EXESig;
  DWORD PEHeaderOffset;
  __int32 PESig;
  IMAGE_FILE_HEADER PEHeader;
  IMAGE_OPTIONAL_HEADER32 OptHeader32;
  IMAGE_OPTIONAL_HEADER64 OptHeader64;
  PPESectionHeaderArray PESectionHeaders;
  __int64 BytesLeft, Bytes;
  BYTE Buf[8191];
  __int64 I;
  DWORD RelocVirtualAddr, RelocPhysOffset, RelocPhysSize;
  __int64 OldSize, NewSize;
  struct __utimbuf64 TimeS;

  PESectionHeaders = NULL;

  {
    RelocPhysOffset = 0;
    RelocPhysSize = 0;
    BackupFilename = Filename + _T(".bak");

    _tcprintf(_T("%s%s"), Filename.c_str(), _T(": "));
    F = _tfopen(Filename.c_str(), _T("rb"));
    std::exception_ptr exc;
    try
    {
      OldSize = FileSize(F);
      struct __stat64 sb;
      _tstat64(Filename.c_str(), &sb);
      TimeS.actime = sb.st_atime;
      TimeS.modtime = sb.st_mtime;

      fread(&EXESig, sizeof(EXESig), 1, F);
      if (EXESig != 0x5A4D) //'MZ'
      {
        throw tstring(_T("File isn't an EXE file (1)."));
        return;
      }
      fseek(F, 0x3C, SEEK_SET);
      fread(&PEHeaderOffset, sizeof(PEHeaderOffset), 1, F);
      if (PEHeaderOffset == 0)
      {
        throw tstring(_T("File isn't a PE file (1)."));
        return;
      }
      fseek(F, PEHeaderOffset, SEEK_SET);
      fread(&PESig, sizeof(PESig), 1, F);
      if (PESig != 0x00004550) //PE\0\0
      {
        throw tstring(_T("File isn't a PE file (2)."));
        return;
      }
      fread(&PEHeader, sizeof(PEHeader), 1, F);
      if (!ForceStrip && ((PEHeader.Characteristics & IMAGE_FILE_DLL) != 0))
      {
        throw tstring(_T("Skipping; can't strip a DLL."));
        return;
      }
      if ((PEHeader.Characteristics & IMAGE_FILE_RELOCS_STRIPPED) != 0)
      {
        throw tstring(_T("Relocations already stripped from file (1)."));
        return;
      }
      PEHeader.Characteristics = PEHeader.Characteristics | IMAGE_FILE_RELOCS_STRIPPED;
      if (PEHeader.SizeOfOptionalHeader != sizeof(OptHeader32))
      {
        if (PEHeader.SizeOfOptionalHeader != sizeof(OptHeader64))
        {
          throw tstring(_T("File isn't a valid 32-bit or 64-bit image (1)."));
          return;
        }
        else
        {
          Is64Bit = true;
        }
      }
      if (Is64Bit)
      {
        fread(&OptHeader64, sizeof(OptHeader64), 1, F);
      }
      else
      {
        fread(&OptHeader32, sizeof(OptHeader32), 1, F);
      }
      if (!Is64Bit && OptHeader32.Magic != IMAGE_NT_OPTIONAL_HDR32_MAGIC)
      {
        throw tstring(_T("File isn't a valid 32-bit image (2)."));
        return;
      }
      if (Is64Bit && OptHeader64.Magic != IMAGE_NT_OPTIONAL_HDR64_MAGIC)
      {
        throw tstring(_T("File isn't a valid 64-bit image (2)."));
        return;
      }
      if (Is64Bit)
      {
        if ((OptHeader64.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress == 0) ||
          (OptHeader64.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size == 0))
        {
          throw tstring(_T("Relocations already stripped from file (2)."));
          return;
        }
        RelocVirtualAddr = OptHeader64.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress;
        OptHeader64.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress = 0;
        OptHeader64.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size = 0;
        if (!WantValidChecksum)
        {
          OptHeader64.CheckSum = 0;
        }
      }
      else
      {
        if ((OptHeader32.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress == 0) ||
          (OptHeader32.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size == 0))
        {
          throw tstring(_T("Relocations already stripped from file (3)."));
          return;
        }
        RelocVirtualAddr = OptHeader32.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress;
        OptHeader32.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress = 0;
        OptHeader32.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size = 0;
        if (!WantValidChecksum)
        {
          OptHeader32.CheckSum = 0;
        }
      }
      PESectionHeaders.reset(new IMAGE_SECTION_HEADER[PEHeader.NumberOfSections]);

      fread(&*PESectionHeaders, PEHeader.NumberOfSections * sizeof(IMAGE_SECTION_HEADER), 1, F);
      for (I = 0; I <= (PEHeader.NumberOfSections - 1); I++)
      {
        IMAGE_SECTION_HEADER *pH = &*PESectionHeaders + I;
        if ((pH->VirtualAddress == RelocVirtualAddr) && (pH->SizeOfRawData != 0))
        {
          RelocPhysOffset = pH->PointerToRawData;
          RelocPhysSize = pH->SizeOfRawData;
          pH->SizeOfRawData = 0;
          break;
        }
      }
      if (RelocPhysOffset == 0)
      {
        throw tstring(_T("Relocations already stripped from file (3)."));
        return;
      }
      if (RelocPhysSize == 0)
      {
        throw tstring(_T("Relocations already stripped from file (4)."));
        return;
      }

      for (I = 0; I <= (PEHeader.NumberOfSections - 1); I++)
      {
        IMAGE_SECTION_HEADER *pH = &*PESectionHeaders + I;
        if (pH)
        {
          if (pH->PointerToRawData > RelocPhysOffset)
          {
            Dec(pH->PointerToRawData, RelocPhysSize);
          }
          if (pH->PointerToLinenumbers > RelocPhysOffset)
          {
            Dec(pH->PointerToLinenumbers, RelocPhysSize);
          }
          if (pH->PointerToRelocations != 0)
          {
            //I don't think this field is ever used in the PE format. StripRlc doesn't handle it.
            throw tstring(_T("Cannot handle this file (1)."));
            return;
          }
        }
      }
      if (Is64Bit)
      {
        if (OptHeader64.ImageBase < 0x400000)
        {
          throw tstring(_T("Cannot handle this file -- the image base address is less than 0x400000."));
          return;
        }
      }
      else
      {
        if (OptHeader32.ImageBase < 0x400000)
        {
          throw tstring(_T("Cannot handle this file -- the image base address is less than 0x400000."));
          return;
        }
      }
    }
    catch (tstring const& )
    {
      exc = std::current_exception();
    }
    fclose(F);
    if (exc) std::rethrow_exception(exc);
    if (FileExists(BackupFilename))
    {
      _tunlink(BackupFilename.c_str());
    }
    _trename(Filename.c_str(), BackupFilename.c_str());
    
    try
    {
      F = _tfopen(BackupFilename.c_str(), _T("rb"));
      try
      {
        F2 = _tfsopen(Filename.c_str(), _T("w+b"), _SH_DENYWR);
        try
        {
          BytesLeft = RelocPhysOffset;
          while (BytesLeft != 0)
          {
            Bytes = BytesLeft;
            if (Bytes > sizeof(Buf))
            {
              Bytes = sizeof(Buf);
            }
            fread(Buf, (size_t)Bytes, 1, F);
            fwrite(Buf, (size_t)Bytes, 1, F2);
            Dec(BytesLeft, Bytes);
          }

          _fseeki64(F, _ftelli64(F) + RelocPhysSize, SEEK_SET);
          BytesLeft = FileSize(F) - _ftelli64(F);
          while (BytesLeft > 0)
          {
            Bytes = BytesLeft;
            if (Bytes > sizeof(Buf))
              Bytes = sizeof(Buf);
            fread(Buf, (size_t)Bytes, 1, F);
            fwrite(Buf, (size_t)Bytes, 1, F2);
            Dec(BytesLeft, Bytes);
          }
          fseek(F2, PEHeaderOffset + sizeof(PESig), SEEK_SET);
          fwrite(&PEHeader, sizeof(PEHeader), 1, F2);
          fwrite((Is64Bit) ? (PVOID)&OptHeader64 : (PVOID)&OptHeader32, (Is64Bit) ? sizeof(OptHeader64) : sizeof(OptHeader32), 1, F2);
          fwrite(&*PESectionHeaders, PEHeader.NumberOfSections * sizeof(IMAGE_SECTION_HEADER), 1, F2);
          if (WantValidChecksum)
          {
            fflush(F2);
            if (Is64Bit)
              OptHeader64.CheckSum = CalcChecksum((HANDLE)_get_osfhandle(_fileno(F2)));
            else
              OptHeader32.CheckSum = CalcChecksum((HANDLE)_get_osfhandle(_fileno(F2)));
            // go back and rewrite opt. header with new checksum 
            fseek(F2, PEHeaderOffset + sizeof(PESig) + sizeof(PEHeader), SEEK_SET);
            fwrite((Is64Bit) ? (PVOID)&OptHeader64 : (PVOID)&OptHeader32, (Is64Bit) ? sizeof(OptHeader64) : sizeof(OptHeader32), 1, F2);
          }
          NewSize = FileSize(F2);

          _futime64(_fileno(F2), &TimeS);
        }
        catch (tstring const &)
        {
          exc = std::current_exception();
        }
        catch (...)
        {
          exc = std::make_exception_ptr(tstring(_T("Unknow error 1")));
        }
        fclose(F2);
        if (exc) std::rethrow_exception(exc);
      }
      catch (tstring const &)
      {
        exc = std::current_exception();
      }
      catch (...)
      {
        exc = std::make_exception_ptr(tstring(_T("Unknow error 2")));
      }
      fclose(F);
      if(exc) std::rethrow_exception(exc);
    }
    catch (tstring const &)
    {
      exc = std::current_exception();
    }
    catch (...)
    {
      exc = std::make_exception_ptr(tstring(_T("Unknow error 3")));
    }
    if (exc)
    {
      _tunlink(Filename.c_str());
      _trename(BackupFilename.c_str(), Filename.c_str());
      std::rethrow_exception(exc);
    }
    _tcprintf(_T("%I64d%s%I64d%s%I64d%s\n"), OldSize, _T(" -> "), NewSize, _T(" bytes ("), OldSize - NewSize, _T(" difference)"));
    if (!KeepBackups)
    {
      if (-1 == _tunlink(BackupFilename.c_str()))
      {
        _tcprintf(_T("Warning: Couldn't delete backup file %s"), BackupFilename.c_str());
      }
    }
  }
}
